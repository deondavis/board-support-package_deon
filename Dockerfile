FROM ubuntu:trusty

RUN apt-get update && apt-get install -y
RUN apt-get install -y \
    gawk \
    wget \
    git-core \
    diffstat \
    unzip \
    texinfo \
    gcc-multilib \
    build-essential \
    chrpath \
    socat \
    libsdl1.2-dev \
    libsdl1.2-dev \
    xterm \
    sed \
    cvs \
    subversion \
    coreutils \
    texi2html \
    docbook-utils \
    python-pysqlite2 \
    help2man \
    make \
    gcc \
    g++ \
    desktop-file-utils \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    mercurial \
    autoconf \
    automake \
    groff \
    curl \
    lzop \
    asciidoc \
    u-boot-tools \
    git 

RUN dpkg --add-architecture i386

RUN apt-get update

RUN apt-get install -y \
    g++-multilib \
    libssl-dev:i386 \
    libcrypto++-dev:i386 \
    zlib1g-dev:i386 \
    bluetooth \
    bluez \
    libbluetooth-dev \
    libudev-dev

RUN locale-gen "en_US.UTF-8"
RUN dpkg-reconfigure locales

RUN git config --global user.name "test"
RUN git config --global user.email "nobody@gmail.com"
RUN git config --global color.ui false 

