------------------------
Rigado Vesta Gateway BSP
------------------------

.. role:: bash(code)
   :language: bash

This document assumes the development environment is being built on a clean install of Ubuntu 14.04 and the user is comfortable with using the Linux command line. Complete the following steps from the Linux command line.

To fetch the latest package available in the Ubuntu repositories run:
    - :bash:`sudo apt-get update`
    - :bash:`sudo apt-get upgrade -y`

To install the essential Yocto Project host packages run the following command:
    - :bash:`sudo apt-get install gawk wget git-core diffstat unzip texinfo gcc-multilib build-essential chrpath socat libsdl1.2-dev libsdl1.2-dev xterm sed cvs subversion coreutils texi2html docbook-utils python-pysqlite2 help2man make gcc g++ desktop-file-utils libgl1-mesa-dev libglu1-mesa-dev mercurial autoconf automake groff curl lzop asciidoc u-boot-tools git`

For compiling for 32-bit ARM with x86_64 host you need to enable x86 packages/libraries with the following commands:
    - :bash:`sudo dpkg --add-architecture i386`
    - :bash:`sudo apt-get update`
    - :bash:`sudo apt-get install g++-multilib libssl-dev:i386 libcrypto++-dev:i386 zlib1g-dev:i386 bluetooth bluez libbluetooth-dev libudev-dev`

To install the :bash:`repo` utility, run the following commands:
    - :bash:`mkdir ~/bin`
    - :bash:`curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo`
    - :bash:`chmod a+x ~/bin/repo`

Add the following line to the :bash:`.bashrc` file to ensure that the :bash:`~/bin` folder is in your :bash:`PATH` variable:
    - :bash:`export PATH=~/bin:$PATH`

Make sure that git is setup properly with the following commands below:
    - :bash:`git config --global user.name "Your Name"`
    - :bash:`git config --global user.email "Your Email"`
    - :bash:`git config –list`

Initialize the repo with the master branch of the repository with the following commands:
    - :bash:`mkdir vesta-gateway-bsp`
    - :bash:`cd vesta-gateway-bsp`
    - :bash:`repo init -u https://deondavis@bitbucket.org/deondavis/board-support-package_deon.git -b boron`
    - :bash:`repo sync -c`

Set up the environment with the following command substituting :bash:`MACHINE_TYPE` for the correct :bash:`MACHINE`:
	- :bash:`MACHINE=<MACHINE TYPE> DISTRO=poky source setup-environment build`
	  
Example with :bash:`MACHINE` type :bash:`vesta-general`:
    - :bash:`MACHINE=vesta-general DISTRO=poky source setup-environment build`

    NOTE: This next instruction will take several hours to build.

To start the build run the following command:
    - :bash:`bitbake vesta-image-developer`

At the last step you may meet an issue with wrong locales:

.. code:: bash
   
    ~/vesta-gateway-bsp/build$ bitbake vesta-image-developer
    Please use a locale setting which supports utf-8.
    Python can't change the filesystem locale after loading so we need a utf-8 when python starts or things won't work.

To fix that you need to issue commands below:

.. code:: bash

    sudo locale-gen "en_US.UTF-8"
    sudo dpkg-reconfigure locales
    export LC_ALL=en_US.UTF-8
    export LANG=en_US.UTF-8
    export LANGUAGE=en_US.UTF-8


**For exsisting users residing in berilliyum branch may skip the above steps and use the exsisting repo as follows:**

Initialize the repo with the master branch of the repository with the following commands:
    - :bash:`cd vesta-gateway-bsp`
    - :bash:`repo init -u https://deondavis@bitbucket.org/deondavis/board-support-package_deon.git -b boron`
    - :bash:`repo sync -c`

Example with :bash:`MACHINE` type :bash:`vesta-general`:
    - :bash:`MACHINE=vesta-general DISTRO=poky source setup-environment build`

To start the build run the following command:
    - :bash:`bitbake vesta-image-developer`


*After a build is complete, the created image will reside in the folder vesta-gateway-bsp/build/tmp/deploy/images. Each image build creates a U-Boot, kernel, and rootfs.* 


