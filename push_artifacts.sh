#!/bin/bash
#Script to push artifacts from Vesta Gateway weekly builds
tar -cvzf weekly-build-$(date +%F).tar.gz artifacts/*
aws s3 cp weekly-build* s3://rigado-gateway-images/private/Weekly-Builds/ 